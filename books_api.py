from pymongo import MongoClient 
from models.books_model import database as database
import csv, json
import json
from bson import ObjectId

db=db()

def objIdToStr(obj):
    return str(obj["_Id"])

def search_book_by_name(**params):
    data_list = []
    for book in db.searchBookByName(**params):
        book["_id"] = objIdToStr(book)
        data_list.append(book)
    print(data_list)
    return data_list

def search_books():
    data_list = []
    for book in db.showBooks():
        book["_id"] = objIdToStr(book)
        data_list.append(book)
    return data_list

def search_books_id(**params):
    data_list = []
    for book in db.showBooks():
        book["_id"] = objIdToStr(book)
        data_list.append(book)
    return data_list

def ubah_data(**params):
    try:
        db.updateBookById(params)
    except Exception as e:
        print(e)

# params = {
#    "id" : "840932jdihefnnjewdf9",
#    "data" : {
#        "tahunterbit":"test_1_tahun",
#        "genre":"test_1_genre"
#    }
#    } 

# search_books_id(**params)